package com.lagou.demo.controller;

import com.lagou.demo.service.IDemoService;
import com.lagou.edu.mvcframework.annotations.LagouAutowired;
import com.lagou.edu.mvcframework.annotations.LagouController;
import com.lagou.edu.mvcframework.annotations.LagouRequestMapping;
import com.lagou.edu.mvcframework.annotations.Security;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@LagouController
@LagouRequestMapping("/demo")
@Security(value = {"P7", "P8", "P9"})
public class DemoController {


    @LagouAutowired
    private IDemoService demoService;


    /**
     * URL: /demo/query?name=lisi
     *
     * @param request
     * @param response
     * @param name
     * @return
     */
    @LagouRequestMapping("/query1")
    @Security(value = {"P9"})
    public String query1(HttpServletRequest request, HttpServletResponse response, String name) throws IOException {
        String s = demoService.get(name);
        response.getWriter().write(s);
        return s;
    }

    @LagouRequestMapping("/query2")
    @Security(value = {"P9", "P8"})
    public String query2(HttpServletRequest request, HttpServletResponse response, String name) throws IOException {
        String s = demoService.get(name);
        response.getWriter().write(s);
        return s;
    }

    @LagouRequestMapping("/query3")
    @Security(value = {"P9", "P8", "P7"})
    public String query3(HttpServletRequest request, HttpServletResponse response, String name) throws IOException {
        String s = demoService.get(name);
        response.getWriter().write(s);
        return s;
    }

    @LagouRequestMapping("/query4")
    @Security(value = {"P10"})
    public String query4(HttpServletRequest request, HttpServletResponse response, String name) throws IOException {
        String s = demoService.get(name);
        response.getWriter().write(s);
        return s;
    }
}

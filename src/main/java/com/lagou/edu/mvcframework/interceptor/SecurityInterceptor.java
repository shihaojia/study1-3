package com.lagou.edu.mvcframework.interceptor;

import com.lagou.edu.mvcframework.annotations.Security;
import com.lagou.edu.mvcframework.pojo.Handler;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Arrays;
import java.util.Map;

public class SecurityInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws IOException {
        // 获取映射求
        Handler handler1 = (Handler) handler;
        // 获取映射器controller上的Security注解
        Security s1 = handler1.getController().getClass().getAnnotation(Security.class);
        // 校验权限
        boolean auth1 = auth(request, s1);
        if (!auth1) {
            response.getWriter().write("401 Unauthorized");
            return false;
        }
        // 获取映射器方法上的Security注解
        Security s2 = handler1.getMethod().getAnnotation(Security.class);
        boolean auth2 = auth(request, s2);
        if (!auth2) {
            response.getWriter().write("401 Unauthorized");
            return false;
        }
        return true;
    }

    /**
     * 校验权限
     *
     * @param request 请求体
     * @param s1      注解
     * @return true
     */
    public boolean auth(HttpServletRequest request, Security s1) {
        if (s1 == null) {
            return true;
        }
        String[] value = s1.value();
        if (value.length == 0) {
            return false;
        }
        // 获取请求username参数
        Map<String, String[]> parameterMap = request.getParameterMap();
        String[] usernames = parameterMap.get("username");
        if (usernames == null || usernames.length == 0) {
            return false;
        }
        String username = usernames[0];
        if (StringUtils.isEmpty(username)) {
            return false;
        }
        // 判断注解value中是否存在
        boolean contains = Arrays.asList(value).contains(username);
        if (contains) {
            return true;
        }
        return false;
    }
}
